import React, { Component } from 'react'
import { shoeArr } from './data_shoeShop'
import ItemShoe from './ItemShoe'
import TableGioHang from './TableGioHang'

export default class Ex_ShoeShop extends Component {
    state = {
        shoeArr: shoeArr,
        gioHang: [],
    };

    renderShoes = () => {
        return this.state.shoeArr.map((item) => {
            return <ItemShoe 
            shoeData={item} 
            key={item.id}
            handleAddToCart={this.handleAddToCart} />
        });
    };

    //viet ham Them San Pham vao gio hang
    handleAddToCart = (sp) => {
        // console.log("yes");

        //tao ra cloneGioHang roi them shoe vao cloneGioHang
        // let cloneGioHang = [...this.state.gioHang, shoe];
        // cloneGioHang.push(shoe);
        // this.setState({
        //     gioHang: cloneGioHang

        
        //});
        //
        //th1: san pham chua co trong gio hang thi them moi san pham va mac dinh so luong trong gio hang la 1
        //sp chua co trong gio hang => tao object moi, co them key soLuong = 1
        //let newSP = {...sp, soLuong: 1};
        //
        //th2: san pham da co trong gio hang thi dua vao vi tri cua no trong gio hang va tim den roi tang so luong len 1
        //sp da co trong gio hang => dua vao index, tang soLuong 
        //gioHang[index].soLuong++

        //dau tien khi nhan nut add to cart thi se tim [index: vi tri] trong gio hang da co san pham do hay chua
        //.find[index = -1] la chua co thi set theo TH1 them moi san pham
        //.find[index != -1] la da co thi set theo TH2 tim toi vi tri san pham va tang soLuong len 1
        //array ngoac vuong, object ngoac nhon

        let index = this.state.gioHang.findIndex((item) => {//tim index
            return item.id == sp.id; //tra ve id gan cho sp
        });
        console.log("index: ", index);
        let cloneGioHang = [...this.state.gioHang]//tao 1 array cloneGioHang copy tu gioHang
        if (index == -1) {//neu index
            let newSp = {...sp, soLuong: 1};
            cloneGioHang.push(newSp);//them moi vao clonGioHang
            //th1
        } else {
            cloneGioHang[index].soLuong++;//da co thi tang soLuong len
            //th2
        }
        this.setState({
            gioHang: cloneGioHang,//thay doi tren giao dien
        });
    };

    handleChangeQuantity = (idShoe, step) => { 
        let index = this.state.gioHang.findIndex((item) => {
            return item.id == idShoe;
            
        });
        
        let cloneGioHang = [...this.state.gioHang]

        cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;

        if (cloneGioHang[index].soLuong == 0) {
            cloneGioHang.splice(index, 1);
        }

        this.setState({gioHang: cloneGioHang,})
        console.log("index",index)
    };

    //viet Ham Xoa san pham trong gio hang
    handleRemoveShoe = (idShoe) => { 
        let index = this.state.gioHang.findIndex((item) => {
            return item.id == idShoe;
        });
        if(index !== -1) {//kiem tra da tim thay id thi moi xoa
            let cloneGioHang = [...this.state.gioHang]
            cloneGioHang.splice(index, 1)
            this.setState({
                gioHang: cloneGioHang,
            })
        }
     }
    
  render() {
    return (
      <div className='container py-5'>
        {this.state.gioHang.length > 0 && <TableGioHang 

            handleRemoveShoe={this.handleRemoveShoe}
            handleChangeQuantity = {this.handleChangeQuantity}
            gioHang={this.state.gioHang} />/*neu gioHang > 0 thi moi render ra giao dien */}
        
        <div className='row'>{this.renderShoes()}</div>
      </div>
    )
  }
}
