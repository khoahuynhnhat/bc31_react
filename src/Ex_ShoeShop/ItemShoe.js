import React, { Component } from "react";
//     id: number;
//     name: string;
//     alias: string;
//     price: number;
//     description: string;
//     shortDescription: string;
//     quantity: number;
//     image: string;
export default class ItemShoe extends Component {
    render() {
        //boc tach phan tu trong shoeData
        let{image, name, description} = this.props.shoeData;
        return (
            <div className="col-3 ">
                <div className="card m-auto" style={{ width: "100%", height: "100%", }}>
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">
                            {description.length<30?description:description.slice(0, 30) + "..."}
                        </p>
                        <button onClick ={() => {this.props.handleAddToCart(this.props.shoeData) }} className="btn btn-primary">add to cart</button>
                    </div>
                </div>
            </div>
        );
    }
}
