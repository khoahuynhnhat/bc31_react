import React, { Component } from 'react'

export default class DemoStateNumber extends Component {
    state ={
        number: 1,
    };
    handlePlusNumber=()=>{
        this.setState
        ({
            number: this.state.number +1,
        });
    };
  render() {
    return (
      <div>
        <p><span>{this.state.number}</span></p>
        <button className='btn btn-success' onClick={this.handlePlusNumber}>Plus number</button>
      </div>
    )
  }
}
