import React, { Component } from 'react'

export default class 
 extends Component {
  render() {
    let tenSanPham = "Iphone 13";
    let linkSanPham = "https://cdn.hoanghamobile.com/i/preview/Uploads/2021/09/15/image-removebg-preview-12.png";
    let renderGiaSanPham = () => {
      return "15.000";
    }
    return (
     <div>
  <div className="card" style={{width: '18rem'}}>
    <img src={linkSanPham} className="card-img-top" alt="..." />
    <div className="card-body">
      {/*inline style: truyen vao object */}
      <h5 style={{
        color: "red",
        backgroundColor: "black",
        
      }}
      className="card-title">{tenSanPham}</h5>
      <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
      <a href="#" className="btn btn-primary">{renderGiaSanPham()}</a>
    </div>
  </div>
</div>

    )
  }
}
