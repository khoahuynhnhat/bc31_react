import React, { Component } from 'react'

export default class DemoState extends 
Component {
    state = {
        isLogin: true,
    };

    handleLogin = () => {
        console.log("login");
        this.setState(
            {
                isLogin: true,
            }
        )
    };

    handleLogout = () => {
        console.log("logout")
        this.setState(
            {
                isLogin: false,
            }
        )
    };
    renderContent = () => {

    }
  render() {
    return (
      <div>
        <p>
            {this.state.isLogin ? (
                <div>
                    <p>Da dang nhap</p>
                    <button className="btn btn-secondary" onClick={this.handleLogout}>Logout</button>
                </div>
            ) : (
            <div>
                <p>Chua dang nhap</p>
                <button className="btn btn-success" onClick={this.handleLogin}>Login</button>
            </div>
            )}
        </p>
      </div>
    );
  }
}
