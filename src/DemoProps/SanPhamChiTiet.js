import React, { Component } from 'react'

export default class SanPhamChiTiet extends Component {
  render() {
    console.log("this.props", this.props)
    return (
      <div>
        <p>Ten san pham: {this.props.detail.name}</p>
        <p>Gia san pham:{this.props.detail.price}</p>
        <p>Ten nguoi dung:{this.props.username}</p>
      </div>
    )
  }
}
