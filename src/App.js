import logo from './logo.svg';
import './App.css';
import DemoClass from './DemoComponent/DemoClass';
import DemoFunction from './DemoComponent/DemoFunction';
import DemoStyle from './DemoStyle/DemoStyle';
import DataBinding from './DataBinding/DataBinding';
import ConditionalRendering from './ConditionalRendering/ConditionalRendering';
import EventBinding from './EventBinding/EventBinding';
import DemoState from './DemoState/DemoState';
import DemoStateNumber from './DemoStateNumber/DemoStateNumber';
import DemoProps from './DemoProps/DemoProps';
import Ex_BaiTapChonXe from './Ex_BaiTapChonXe/Ex_BaiTapChonXe';
import RenderWithMap from './RenderWithMap/RenderWithMap';
import Ex_ShoeShop from './Ex_ShoeShop/Ex_ShoeShop';


function App() {
  return (
    <div className="App">
     {/* <DemoClass></DemoClass>
     <DemoClass />
    <DemoFunction /> */}
    {/* <DemoStyle />
    <div className="demo_title">Hello</div> */}
    {/* <DataBinding /> */}
    {/* <ConditionalRendering /> */}
    {/* <EventBinding /> */}
    {/* <DemoState /> */}
    {/* <DemoStateNumber /> */}
    {/* <DemoProps /> */}
    {/* <Ex_BaiTapChonXe /> */}
    {/* <RenderWithMap /> */}
    <Ex_ShoeShop />
    </div>
  );
}

export default App;
