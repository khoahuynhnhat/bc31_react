import React, { Component } from "react";

export default class ItemMovie extends Component {
    render() {
        return (
            <div className="card" style={{ width: "18rem" }}>
                <img src={this.props.movie.hinhAnh} className="card-img-top" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">{this.props.movie.tenPhim}</h5>
                    <p className="card-text">
                        {this.props.movie.moTa}
                    </p>
                    <a href="#" className="btn btn-primary">
                        Go somewhere
                    </a>
                </div>
            </div>
        );
    }
}
