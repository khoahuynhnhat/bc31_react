import React, { Component } from 'react'

//import file css
import "./demoStyle.css"

import style from "./demoStyle.module.css"
export default class DemoStyle extends Component {
  render() {
    return (
      <div className={style.demo_title}>DemoStyle</div>
    )
  }
}
